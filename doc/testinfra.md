
"Infrastructure" de test

* C'est quoi ?
* pour quoi faire  ?
* Inconvenients
* Avantages


# c'est quoi ?

un niveau intermédiare entre le script de test déroulant un scenario du plan de test
et la la plate-forme des équipements sous test.

l'infrastructure de test fournit des primimitives (services) de haut niveau
qui seront utilisés par les scripts de tests.


# pour quoi faire ?

## simplifier l'écriture des scripts unitaires de test.

    grande quantité de test à ecrire (plusieurs centaines)
    facilité d'ajouter de nouveaux tests pour
    prendre en compte une forte combinatoire

    s'adapter plus facilement à un changement de langage de scripts ( RF -> cucumber )

    Lisibilité des tests


## mutualiser des procédures ou services

    standardisation des tests
    maintenance facilitée
    Evolutivité des routines de tests , sans impacts sur les sripts déjà écrits.


## Superviser l'etat de plateforme avant test
## reserver des resources avant test


 # Inconvenients:

    2 profils de développeurs de tests

    1 dev de scripts ( robot-framework ...) prenant en charge le scenario de test et la combinatoire

    1 dev de services ( python / GO etc.. )



 # Avantages:

    * Maintenabilité
    * Supervision

    les services sont "pérennisés" dans un langage de référence ( python / go ...)
    evolutions des services "retroactifs" pour les scripts de tests deja ecrits.

    services ecrits dans un langage de programmation reconnu ( python / GO ) et ses librairies
        et de son outillage ( IDE , DEBUGGER , git versionnage librairies )




# cas d'usage

verifier la sequence DORA ( dhpc V4) lors du reboot d'une livebox .


Approche directe:

  depuis le script en RF:
    se connecter à une machine pour déclencher une trace réseau
    se connecter à la box pour declencher un reboot
    ATTENDRE , un temps arbitraire correspondant à la plus longue période éstimée
    Arreter la trace réseau distante
    Rapatrier le fichier pcap
    analyser le fichier PCAP pour detecter les differentes section composant DORA


Approche "infrastructure"

 scanner
    un service  DhcpScanner permanent "ecoute" sur le réseau en afin de detecter tous les evenements de type
    DHCPV4.
    pour chacun de ces evenements emet un message structuré DHCP sur un bus de message (nats)

 aggregateur
    un service s'abonnant aux evenements DHCP du scanner est chargé de reconnaitre les sequences DORA
    pour chaque equipement et emet un message DORA pour chacun.


 service REST DORA
    interrogeable par le script primaire.
    s abonne au flux "aggregateur DORA pour un box donnée"
    et répond s'il a recu ou non le DORA dans le temps imparti.


 service REST REBOOT
    Gére le reseau de PDU pour Allumer/Eteindre une box en particulier


 script unititaire en RF ou autre:

    Rebooter livebox "X"
    Attendre DORA sur livevebox "X" , avec un timeout de 3 mn
    analyse du resultat: OK ou timeout ?