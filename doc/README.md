
# netscan ( network sniffer )

a service listening to an interface ( or pcap file ) and publish nats events relative to dhcp
 or radius on.




# netscan launch

    $sudo netscan --help

    sniffer 0.2
    Scan a network interface OR a pcap file and send message to nats server OR screen
    sample usage : netscan --name s1 --interface eth1 --nats nats://<host>:4222
    options:
    --name string
    	name of the sniffer (default "0") (eg 0 , will publish to sniffer.0.>)

    --interface string
        	network interface (default "eth1")
    --pcap string
    		pcap file to read (eg: sample.pcap)

    --nats string
        	nats server (default "nats://127.0.0.1:4222")
    --screen
        	publish to screen


## example
    sudo netscan --name ne --nats nats://127.0.0.1:4222 --interface NE


# netscan dhcp output samples

subject: sniffer.<name>.<mac>.dncp.<tag>.<sequence>

screenpublisher: publish to [sniffer.ne.02:ff:ff:e0:88:41.dhcp.Discover.17] the message:
{"Kind":"dhcp","dhcpOp":"Request","ClientHWAddr":"02:ff:ff:e0:88:41","SrcMAC":"a4:7b:2c:e4:d7:77","DstMAC":"3c:61:04:37:0c:fe","SrcIP":"193.253.21.177","DstIP":"172.20.123.118","IPLength":483,"UdpLength":463,"SrcPort":67,"DstPort":67,"Event":{"ClientHWAddr":"02:ff:ff:e0:88:41","DHCPAuthentication":"\u0000\u0000\u0000\u001a\t\u0000\u0000\u0005X\u0001\u0003A\u0001\rfti/v2wyh42\u003c\u0012tnKpmfiA\u0026A\u003c*JZQ|\u0003\u0013o\u003e\ufffd呖2","DHCPCircuitID":"DSLAN481!1!efm!1/33:8:32","DHCPMsgType":"Discover","DHCPRemoteID":"00000018773002","VendorClassIdentifier":"sagem","dhcpOp":"Request","fti":"fti/v2wyh42"},"fti":"fti/v2wyh42"}

.../...

screenpublisher: publish to [sniffer.ne.02:ff:ff:e0:88:41.dhcp.Offer.18] the message:
{
    "Kind":"dhcp",
    "dhcpOp":"Reply",
    "ClientHWAddr":"02:ff:ff:e0:88:41",
    "SrcMAC":"3c:61:04:37:0c:fe",
    "DstMAC":"a4:7b:2c:e4:d7:77",
    "SrcIP":"172.20.123.118",
    "DstIP":"193.253.21.177",
    "IPLength":525,
    "UdpLength":505,
    "SrcPort":67,
    "DstPort":67,
    "Event":{
        "AutoFallBack":false,
        "ClientHWAddr":"02:ff:ff:e0:88:41",
        "DHCPAuthentication":"\u0000\u0000\u0000dhcpli",
        "DHCPCircuitID":"DSLAN481!1!efm!1/33:8:32",
        "DHCPMsgType":"Offer","DHCPRemoteID":"00000018773002",
        "ForcedPPP":false,
        "IPTVRunningInterface":"0x1",
        "dhcpOp":"Reply"
    }
}

.../...

screenpublisher: publish to [sniffer.ne.02:ff:ff:e0:88:41.dhcp.Request.21] the message:
{
    "Kind":"dhcp",
    "dhcpOp":"Request",
    "ClientHWAddr":"02:ff:ff:e0:88:41",
    "SrcMAC":"a4:7b:2c:e4:d7:77","DstMAC":"3c:61:04:37:0c:fe","SrcIP":"193.253.21.177","DstIP":"172.20.123.118",
    "IPLength":495,"UdpLength":475,"SrcPort":67,"DstPort":67,
    "Event":{
        "ClientHWAddr":"02:ff:ff:e0:88:41",
        "DHCPAuthentication":"\u0000\u0000\u0000\u001a\t\u0000\u0000\u0005X\u0001\u0003A\u0001\rfti/v2wyh42\u003c\u0012tnKpmfiA\u0026A\u003c*JZQ|\u0003\u0013o\u003e\ufffd呖2",
        "DHCPCircuitID":"DSLAN481!1!efm!1/33:8:32",
        "DHCPMsgType":"Request",
        "DHCPRemoteID":"00000018773002",
        "VendorClassIdentifier":"sagem",
        "dhcpOp":"Request",
        "fti":"fti/v2wyh42"
    },
    "fti":"fti/v2wyh42"
}

screenpublisher: publish to [sniffer.ne.02:ff:ff:e0:88:41.dhcp.Ack.22] the message:
{"Kind":"dhcp","dhcpOp":"Reply","ClientHWAddr":"02:ff:ff:e0:88:41","SrcMAC":"3c:61:04:37:0c:fe","DstMAC":"a4:7b:2c:e4:d7:77","SrcIP":"172.20.123.118","DstIP":"193.253.21.177","IPLength":525,"UdpLength":505,"SrcPort":67,"DstPort":67,"Event":{"AutoFallBack":false,"ClientHWAddr":"02:ff:ff:e0:88:41","DHCPAuthentication":"\u0000\u0000\u0000dhcpli","DHCPCircuitID":"DSLAN481!1!efm!1/33:8:32","DHCPMsgType":"Ack","DHCPRemoteID":"00000018773002","ForcedPPP":false,"IPTVRunningInterface":"0x1","dhcpOp":"Reply"}}



# netscan radius output samples

subject: sniffer.<name>.<mac>.radius.<tag>.<sequence>

screenpublisher: publish to [sniffer.ne.a4:7b:2c:e4:d7:77.radius.AccessRequest.0] the message:
{"Kind":"radius","SrcMAC":"a4:7b:2c:e4:d7:77","DstMAC":"3c:61:04:37:0c:fe","SrcIP":"172.20.23.174","DstIP":"172.20.1.114","IPLength":91,"UdpLength":71,"SrcPort":64396,"DstPort":1645,"Event":{"NAS-IP-Address":"172.20.23.174","PacketId":150,"RadiusClass":"Authentication","RadiusOp":"AccessRequest","User-Name":"P1Z1X2C7S9Y9B0O8`","User-Password":"9n9Rk1aslzM9GISBztWwPA=="}}

screenpublisher: publish to [sniffer.ne.a4:7b:2c:e4:d7:77.radius.AccessReject.1] the message:
{"Kind":"radius","SrcMAC":"3c:61:04:37:0c:fe","DstMAC":"a4:7b:2c:e4:d7:77","SrcIP":"172.20.1.114","DstIP":"172.20.23.174","IPLength":53,"UdpLength":33,"SrcPort":1645,"DstPort":64396,"Event":{"PacketId":150,"RadiusClass":"Authentication","RadiusOp":"AccessReject","Reply-Message":"nil"}}

screenpublisher: publish to [sniffer.ne.a4:7b:2c:e4:d7:77.radius.AccessRequest.2] the message:
{
    "Kind":"radius",
    "SrcMAC":"a4:7b:2c:e4:d7:77",
    "DstMAC":"3c:61:04:37:0c:fe",
    "SrcIP":"172.20.23.174",
    "DstIP":"172.20.1.114",
    "IPLength":91,
    "UdpLength":71,
    "SrcPort":64397,
    "DstPort":1645,
    "Event":{
        "NAS-IP-Address":"172.20.23.174",
        "PacketId":151,
        "RadiusClass":"Authentication",
        "RadiusOp":"AccessRequest",
        "User-Name":"P1Z1X2C7S9Y9B0O8a",
        "User-Password":"7aYmLwXJ4Ow1BajNxGfxpw=="
    }
}




# subscriptions examples

## sniffer.ne.a4:7b:2c:e4:d7:77.>

subscribes to all events (dhcp and radius) of sniffer "ne" filtered by mac address a4:7b:2c:e4:d7:77


## sniffer.ne.*.dhcp.>

subscribes to all dhcp events of sniffer "ne" for all mac address


## sniffer.ne.*.radius.>

subscribes to all radius events of sniffer "ne" for all mac address
