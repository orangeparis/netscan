package test

import (
	natsserver "github.com/nats-io/nats-server/test"

	"testing"
	"time"
)

/*

	to start a nats server for testing

	Usage:
			see TestSample function




*/

func TestSample(t *testing.T) {

	// start a nats server for testing and shutdown it at exit
	s := natsserver.RunDefaultServer()
	defer s.Shutdown()

	// do your test here

	time.Sleep(2 * time.Second)

}

//////////////////////////////////////////////////////////////////////////////////
//// Running nats server in separate Go routines
//////////////////////////////////////////////////////////////////////////////////
//
//// RunDefaultServer will run a server on the default port.
//func RunDefaultServer() *server.Server {
//	return RunServerOnPort(nats.DefaultPort)
//}
//
//// RunServerOnPort will run a server on the given port.
//func RunServerOnPort(port int) *server.Server {
//	opts := natsserver.DefaultTestOptions
//	opts.Port = port
//	return RunServerWithOptions(opts)
//}
//
//// RunServerWithOptions will run a server with the given options.
//func RunServerWithOptions(opts server.Options) *server.Server {
//	return natsserver.RunServer(&opts)
//}
//
//// RunServerWithConfig will run a server with the given configuration file.
//func RunServerWithConfig(configFile string) (*server.Server, *server.Options) {
//	return natsserver.RunServerWithConfig(configFile)
//}
