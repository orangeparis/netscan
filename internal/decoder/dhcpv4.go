package decoder

import (
	"bitbucket.org/orangeparis/ines/publisher"
	"bitbucket.org/orangeparis/netscan/internal"
	"fmt"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"log"
)

/*

	a dhcpv4 decoder

		decode dhcp options 125, 82 , 90


The Livebox MUST provide the DHCP Server with an "Authentication information" parameter in
DHCP option 90 sent in the DHCP DISCOVER/REQUEST Messages.
Replay Detection is not controlled.
Only the simple configuration token mode where Authentication information is unencoded (Protocol
field=0, Algorithm=0 and RDM=0) as defined in [RFC 3118] is supported.
The PPP login (fti/xxxxx) and Password information are sent in "Authentication information"
parameter based on a CHAP [RFC 1994] like method as defined below.
The content of authentication information field is built using a Radius based [RFC 2865] TLV
encoding method specifying:
- Orange Encryption method (T=26, L=9, Vendor-id=0x00000558 T=1, L=3, Value=A)
o 0x00000558 is the hexadecimal value of Orange Vendor Id (1368 registred at
IANA)

- Username (T=1, L= Length of the Username field, Value=unencrypted PPP login
- Challenge (T=60, L=18, Value= Random)
Chap password (T=3, L=19, Value= Chap-Id + encrypted PPP password
DHCP Option 90 content is the same as the DHCPv6 Option11 (see [R10])



*/

var dhcpV4Name = "dhcpv4"
var dhcpv4BpfFilter string = "portrange 67-68 or  (vlan && portrange 67-68)"

type DhcpV4Decoder struct {
	name            string
	bpfFilter       string
	Publisher       publisher.Publisher
	MessageSequence int
	ports           []int
}

func NewDhcpV4Decoder(pub publisher.Publisher) (decoder *DhcpV4Decoder, err error) {

	ports := []int{67, 68}
	decoder = &DhcpV4Decoder{name: dhcpV4Name, bpfFilter: dhcpv4BpfFilter, Publisher: pub, ports: ports}

	return
}

// implements decoder interface
func (d *DhcpV4Decoder) Name() string {
	return d.name
}

func (d *DhcpV4Decoder) Ports() []int {
	return d.ports
}

func (d *DhcpV4Decoder) Bpf() string {
	return ComputeBpf(d.ports)
}

func (d *DhcpV4Decoder) Decode(event *internal.PacketEvent) bool {

	if event.IsDHCPv4 == false {
		// no dhcpv4 layer -> return not done
		return false
	}
	event.Kind = "dhcp"

	event.Event["dhcpOp"] = event.Layers.DHCPv4.Operation.String()
	event.Event["ClientHWAddr"] = event.Layers.DHCPv4.ClientHWAddr.String()
	// event["Xid"] = dhcp.Xid
	//options := dhcp.Options.String()
	//fmt.Printf("options:%s\n", options)
	for _, o := range event.Layers.DHCPv4.Options {
		if o.Type == layers.DHCPOptMessageType {
			msgType := layers.DHCPMsgType(o.Data[0])
			event.Event["DHCPMsgType"] = msgType.String()
			if event.Event["DHCPMsgType"] == "Ack" {
				//fmt.Println("Dhcp Reply Ack")
			}
		}
		if o.Type == 125 {
			// orange option livebox 125  AutoFallBack and ForcedPPP
			data := o.Data
			A := data[7]
			T := data[8]
			P := data[9]
			var CC = [2]byte{data[10], data[11]}
			if A == 1 {
				event.Event["AutoFallBack"] = true
			} else {
				event.Event["AutoFallBack"] = false
			}
			if P == 1 {
				event.Event["ForcedPPP"] = true
			} else {
				event.Event["ForcedPPP"] = false
			}
			event.Event["IPTVRunningInterface"] = fmt.Sprintf("%#v", T)
			_ = T
			_ = CC

		}
		if o.Type == 82 {
			// generic option: DHCP Relay Agent Information Option
			// Ciruit-ID , Remote-ID , Vendor-ID

			relayAgentInformation := "yes"
			_ = relayAgentInformation
			offset := 0
			subOp := int(o.Data[offset])
			offset++
			l := int(o.Data[offset])
			offset++
			if l > 0 {
				data := o.Data[offset : l+offset]
				event.Event["DHCPCircuitID"] = string(data)
				//println(string(data))
				_ = subOp
				_ = data
				// next
				offset += l
				if offset < int(o.Length) {
					subOp2 := int(o.Data[offset])
					offset++
					l2 := int(o.Data[offset])
					offset++
					if l2 > 0 {
						data2 := o.Data[offset : l2+offset]
						event.Event["DHCPRemoteID"] = string(data2)
						//println(string(data2))
						offset += l2
						_ = data2
						_ = subOp2
					}
				}
			}

		}
		if o.Type == 90 {
			// Authentication for DHCP Request Messages (DISCOVER/REQUEST )
			//   code   Length   protocol  algorythm
			//   rdm    -- replay detection -------
			//   ----   -- authentification --------
			//    -----------
			//println("dhcp option 90")

			protocol := o.Data[0]
			algorythm := o.Data[1]
			rdm := o.Data[2]
			replay := o.Data[3:7]
			authent := o.Data[8 : len(o.Data)-10]
			_ = protocol
			_ = algorythm
			_ = rdm
			_ = replay
			_ = authent
			//println("dhcp authent " + string(authent))
			event.Event["DHCPAuthentication"] = string(authent)
			if len(authent) >= 25 {
				event.Event["fti"] = string(authent[14:25])
				event.Fti = string(authent[14:25]) // added for sniffer v1 compatibility
			}
		}
		if o.Type == 60 {
			// vendor class identifier  eg sagem ...
			//length := o.Data[0]
			//_=length
			event.Event["VendorClassIdentifier"] = string(o.Data)
		}
		//fmt.Printf("option: %s\n", o.String())
	}
	d.PublishEvent(*event)
	return true
}

// private decoder functions
func (d *DhcpV4Decoder) PublishEvent(event internal.PacketEvent) {

	// compute topic
	var topic string

	op := event.Layers.DHCPv4.Operation.String()
	dhcpType, ok := event.Event["DHCPMsgType"]
	if !ok {
		// dont have a msgtype (Discover/Offer/Request/Ack get the operation instead (Request/Reply)
		dhcpType = op
	}

	// dhcp.ClientHWAddr
	if event.Event["ClientHWAddr"] != "" {
		// we got a dhcp client address => make the topic with it
		topic = fmt.Sprintf("%s.dhcp.%s", event.Event["ClientHWAddr"], dhcpType)
	} else {
		// we dont have a dhcp client

		// by default publish on srcMac channel: <SrcMac>.dhcp.<op>
		topic = fmt.Sprintf("%s.dhcp.%s", event.Layers.Ethernet.SrcMAC.String(), op)
		switch op {
		case "Request":
			// assume it is a livebox request send a dhcp message   root.<srcMac>.dhcp.<op>
			//topic = fmt.Sprintf("%s.dhcp.%s", eth.SrcMAC.String(), op)
		case "Reply":
			// assume it is a dhcp reply to the livebox send a dhcp message root.<DstMac>.dhcp.<op>
			topic = fmt.Sprintf("%s.dhcp.Reply", event.Layers.Ethernet.DstMAC.String())
		default:
			// unkwonw dhcp type ???? should not happen
			log.Printf("dhcp filter: unknown type: %s", op)
		}
	}

	// add sequence to topic
	topic = fmt.Sprintf("%s.%d", topic, d.MessageSequence)
	d.MessageSequence += 1

	msg, err := event.Dumps()
	if err != nil {
		log.Printf("dhcpv4 failed to marshal event: %s\n", err)
		return
	}
	// publish the dchpv4 event
	err = d.Publisher.Publish(topic, msg)
	if err != nil {
		// increment sequence
		log.Printf("dhcpv4 failed to send event: %s\n", err)
	}

}

func (d *DhcpV4Decoder) Run(source *gopacket.PacketSource) {

	parser := internal.NewPacketParser()
	for packet := range source.Packets() {

		event, err := parser.Parse(packet.Data())
		if err != nil {
			//fmt.Printf("%s\n", err)
			continue
		}

		found := d.Decode(event)
		if found == false {
			//println("not found")
			_ = found
		}
	}
}
