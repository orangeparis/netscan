package decoder

import (
	"bitbucket.org/orangeparis/ines/publisher"
	"bitbucket.org/orangeparis/netscan/internal"
	"bitbucket.org/orangeparis/netscan/internal/packets"
	"fmt"
	"testing"
)

func TestNewDhcpV4Decoder(t *testing.T) {

	pcapFile := "../../test/fixtures/ines_dhcp.pcap"

	// create a publisher
	pub := publisher.NewScreenPublisher("sniffer.ne")

	// create the dhcp decoder
	d, err := NewDhcpV4Decoder(pub)
	if err != nil {
		fmt.Printf("%s\n", err)
		t.Fail()
		return
	}
	bpf := d.Bpf()

	// create a packet source
	source, err := packets.MakePacketSourceFromPcap(pcapFile, bpf)
	if err != nil {
		fmt.Printf("%s\n", err)
		t.Fail()
		return
	}

	parser := internal.NewPacketParser()
	for packet := range source.Packets() {

		event, err := parser.Parse(packet.Data())
		if err != nil {
			fmt.Printf("%s\n", err)
			continue
		}

		// decode packet
		done := d.Decode(event)
		//if done == true {
		//	j,err := event.Dumps()
		//	if err != nil {
		//		fmt.Printf("%s\n",err)
		//		t.Fail()
		//		return
		//	}
		//	pub.Publish("dhcp",j)
		//}
		//println(done)
		_ = done
	}

}
