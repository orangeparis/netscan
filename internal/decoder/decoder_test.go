package decoder

import (
	"bitbucket.org/orangeparis/ines/publisher"
	"bitbucket.org/orangeparis/netscan/internal/packets"
	"fmt"
	"testing"
)

func TestBpfHelper(t *testing.T) {

	ports := []int{67, 68}

	bpf := ComputeBpf(ports)

	if bpf != `(port 67 || port 68) || ( vlan && (port 67 || port 68))` {
		t.Fail()
		return
	}
	println(bpf)

}

func TestDecoderChainBpf(t *testing.T) {

	// create a publisher
	pub := publisher.NewScreenPublisher("sniffer.1")

	// create the dhcp decoder
	dhcp, err := NewDhcpV4Decoder(pub)
	if err != nil {
		fmt.Printf("%s\n", err)
		t.Fail()
		return
	}

	// create a radius decoder
	radius, err := NewRadiusDecoder(pub)
	if err != nil {
		fmt.Printf("%s\n", err)
		t.Fail()
		return
	}

	// create a decoder chain
	chain := &Chain{}
	chain.AddMembers(dhcp, radius)

	bpf := chain.Bpf()
	if bpf != `(port 67 || port 68 || port 1645 || port 1646) || ( vlan && (port 67 || port 68 || port 1645 || port 1646))` {
		t.Fail()
		return
	}

	println(bpf)

}

func TestDecoderChainRun(t *testing.T) {

	pcapFile := "../../test/fixtures/ines_dhcp.pcap"

	// create a publisher
	pub := publisher.NewScreenPublisher("sniffer.1")

	// create the dhcp decoder
	dhcp, err := NewDhcpV4Decoder(pub)
	if err != nil {
		fmt.Printf("%s\n", err)
		t.Fail()
		return
	}
	_ = dhcp

	// create a radius decoder
	radius, err := NewRadiusDecoder(pub)
	if err != nil {
		fmt.Printf("%s\n", err)
		t.Fail()
		return
	}
	_ = radius

	// create a decoder chain with dhcp and radius decoder
	chain := &Chain{}
	chain.AddMembers(dhcp, radius)

	// compute bpf for the decoder chain
	bpf := chain.Bpf()

	// create a packet source from a pcap file
	source, err := packets.MakePacketSourceFromPcap(pcapFile, bpf)
	if err != nil {
		fmt.Printf("%s\n", err)
		t.Fail()
		return
	}

	chain.Run(source)

}
