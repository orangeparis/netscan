package decoder

import (
	"bitbucket.org/orangeparis/ines/publisher"
	"bitbucket.org/orangeparis/netscan/internal"
	"fmt"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"log"
)

/*

	a dhcpv6 decoder




*/

var dhcpV6Name = "dhcpv6"
var dhcpv6BpfFilter string = "portrange 546-547 or  (vlan && portrange 546-547)"

type DhcpV6Decoder struct {
	name            string
	bpfFilter       string
	Publisher       publisher.Publisher
	MessageSequence int
	ports           []int
}

func NewDhcpV6Decoder(pub publisher.Publisher) (decoder *DhcpV6Decoder, err error) {

	ports := []int{546, 547}
	decoder = &DhcpV6Decoder{name: dhcpV6Name, bpfFilter: dhcpv6BpfFilter, Publisher: pub, ports: ports}

	return
}

// implements decoder interface
func (d *DhcpV6Decoder) Name() string {
	return d.name
}

func (d *DhcpV6Decoder) Ports() []int {
	return d.ports
}

func (d *DhcpV6Decoder) Bpf() string {
	return ComputeBpf(d.ports)
}

func (d *DhcpV6Decoder) Decode(event *internal.PacketEvent) bool {

	if event.IsDHCPv6 == false {
		// no dhcpv6 layer -> return not done
		return false
	}
	event.Kind = "dhcpv6"

	event.Event["DhcpOp"] = event.Layers.DHCPv6.MsgType.String()
	//fmt.Printf("MsgType: %s\n", event.Event["DhcpOp"])

	for _, o := range event.Layers.DHCPv6.Options {

		code := o.Code.String()
		_ = code
		if o.Code == layers.DHCPv6OptRelayMessage {
			//fmt.Printf("option: %s\n", o.Code.String())
			//fmt.Printf("msgtype: %s\n", string(o.Data[0]))

			mt := layers.DHCPv6MsgType(o.Data[0])
			MsgType := mt.String()
			//event.Event["DhcpOp"] = MsgType
			event.Event["DHCPMsgType"] = MsgType
			//if mt == layers.DHCPv6MsgTypeSolicit {
			//	t := mt.String()
			//	fmt.Printf("type: %s\n", t)
			//	//print("SOLICIT")
			//}

		}

		//fmt.Printf("option: %s\n", o.Code.String())
		//fmt.Printf("data: %s\n", o.Data)

	}
	d.PublishEvent(*event)
	return true
}

// private decoder functions
func (d *DhcpV6Decoder) PublishEvent(event internal.PacketEvent) {

	// compute topic
	var topic string

	//op := event.Layers.DHCPv4.Operation.String()
	dhcpType, ok := event.Event["DHCPMsgType"]
	if !ok {
		// dont have a msgtype (Discover/Offer/Request/Ack get the operation instead (Request/Reply)
		dhcpType = "NoDHCPMsgType"
	}
	//op := dhcpType

	//event.Event["ClientHWAddr"] = event.Layers.DHCPv6.PeerAddr.String()
	event.Event["ClientHWAddr"] = event.SrcMAC
	if event.Event["DhcpOp"] == "RelayReply" {
		event.Event["ClientHWAddr"] = event.DstMAC
	}
	topic = fmt.Sprintf("%s.dhcpv6.%s", event.Event["ClientHWAddr"], dhcpType)

	/*
		// dhcp.ClientHWAddr
		if event.Event["ClientHWAddr"] != "" {
			// we got a dhcp client address => make the topic with it
			topic = fmt.Sprintf("%s.dhcp.%s", event.Event["ClientHWAddr"], dhcpType)
		} else {
			// we dont have a dhcp client

			// by default publish on srcMac channel: <SrcMac>.dhcp.<op>
			topic = fmt.Sprintf("%s.dhcp.%s", event.Layers.Ethernet.SrcMAC.String(), op)
			switch op {
			case "Request":
				// assume it is a livebox request send a dhcp message   root.<srcMac>.dhcp.<op>
				//topic = fmt.Sprintf("%s.dhcp.%s", eth.SrcMAC.String(), op)
			case "Reply":
				// assume it is a dhcp reply to the livebox send a dhcp message root.<DstMac>.dhcp.<op>
				topic = fmt.Sprintf("%s.dhcp.Reply", event.Layers.Ethernet.DstMAC.String())
			default:
				// unkwonw dhcp type ???? should not happen
				log.Printf("dhcp filter: unknown type: %s", op)
			}
		}

	*/

	// add sequence to topic
	topic = fmt.Sprintf("%s.%d", topic, d.MessageSequence)
	d.MessageSequence += 1

	msg, err := event.Dumps()
	if err != nil {
		log.Printf("dhcpv6 failed to marshal event: %s\n", err)
		return
	}
	// publish the dchpv4 event
	err = d.Publisher.Publish(topic, msg)
	if err != nil {
		// increment sequence
		log.Printf("dhcpv6 failed to send event: %s\n", err)
	}

}

func (d *DhcpV6Decoder) Run(source *gopacket.PacketSource) {

	parser := internal.NewPacketParser()
	for packet := range source.Packets() {

		event, err := parser.Parse(packet.Data())
		if err != nil {
			//fmt.Printf("%s\n", err)
			continue
		}

		found := d.Decode(event)
		if found == false {
			//println("not found")
			_ = found
		}
	}
}
