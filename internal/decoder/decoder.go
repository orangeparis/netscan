package decoder

import (
	"bitbucket.org/orangeparis/netscan/internal"
	"fmt"
	"github.com/google/gopacket"
	"strconv"
	"strings"
)

/*




 */

// Decoder : Decode an event eg Dhcpv4 decoder
type Decoder interface {
	Name() string
	Decode(e *internal.PacketEvent) (done bool) // decode the given event , return false if no patch
	Bpf() string                                // return the default ebpf filter
	Ports() []int                               // list of udp ports for building bpf
}

// a set of decoders
type Chain struct {
	Members []Decoder
}

func (c *Chain) AddMembers(member ...Decoder) {
	for _, m := range member {
		c.Members = append(c.Members, m)
	}
	//c.Members = append(c.Members, member)
}

// Bpf compute the bpf as the union of all members bpf
func (c *Chain) Bpf() (bpf string) {

	if len(c.Members) <= 0 {
		return ""
	}

	var all []int

	for _, member := range c.Members {
		for _, p := range member.Ports() {
			all = append(all, p)
		}
	}
	bpf = ComputeBpf(all)
	return bpf

}

// chain decode : performs decode with each member decoder
func (c *Chain) Decode(event *internal.PacketEvent) bool {

	var done bool
	// decode packet
	for _, decoder := range c.Members {
		done = decoder.Decode(event)
		if done == true {
			break
		}
	}
	return done
}

func (c *Chain) Run(source *gopacket.PacketSource) {

	parser := internal.NewPacketParser()
	for packet := range source.Packets() {

		event, err := parser.Parse(packet.Data())
		if err != nil {
			fmt.Printf("%s\n", err)
			continue
		}

		found := c.Decode(event)
		if found == false {
			println("not found")
		}
	}
}

// helpers

// return the bpf string eg ( port 67 || port 68 ) || ( vlan && ( port 67 || port 68) )
func ComputeBpf(ports []int) (bpf string) {

	var strPorts []string
	for _, x := range ports {
		strPorts = append(strPorts, "port "+strconv.Itoa(x))
	}
	p := "(" + strings.Join(strPorts, " || ") + ")"
	v := "( vlan && " + p + ")"

	bpf = p + " || " + v
	return bpf

}
