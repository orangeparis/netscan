package decoder

import "bytes"

/*

	vendor specific fields

	Vendor-Specific":"AAAZfwsQMDAwMDAwMTc4OTkwMDYMHnByb2ZpbC1zdWItZGVmYXVsdC1NU0FOLXhEU0wNHnByb2ZpbC1zbGEtZGVmYXVsdC1NU0FOLXhEU0yjBgAAAAE="}}


..00000019633007profil-sub-default-MSAN-xDSL
profil-sla-default-MSAN-xDSL�...


�DSLAN286!11!efm!07/14:83200000019633007



..00000019633007profil-sub-default-MSAN-xDSL
profil-sla-default-MSAN-xDSL...


DSLAN286!11!efm!07/14:83200000019633007


*/func OrangeVendorDecode(data []byte) {

	//fmt.Printf("\n%s\n",data)

	OrangeAuthenticationAlgorithm := data[0]
	OrangeMessageType := data[1]
	OrangeUserClass := data[2]
	OrangeDomainSearchv4 := data[3]
	OrangeDomainSearchv6 := data[4]

	Rest := data[5:]

	El := bytes.Split(Rest, []byte("!"))
	x := len(El)
	_ = x

	_ = OrangeAuthenticationAlgorithm
	_ = OrangeMessageType
	_ = OrangeUserClass
	_ = OrangeDomainSearchv4
	_ = OrangeDomainSearchv6
	_ = Rest

	return
}
