package decoder

import (
	"bitbucket.org/orangeparis/ines/publisher"
	"bitbucket.org/orangeparis/netscan/internal"
	"bitbucket.org/orangeparis/netscan/internal/packets"
	"fmt"
	"testing"
)

func TestNewRadiusDecoder(t *testing.T) {

	pcapFile := "../../test/fixtures/ines_dhcp.pcap"
	//pcapFile := "../..//test/fixtures/lbreboot_ne_bootpAndRadius.pcap"
	//pcapFile := "../..//test/fixtures/RADIUS.cap"
	//pcapFile := "../../test/fixtures/traces_radius.pcapng"

	// create a publisher
	pub := publisher.NewScreenPublisher("sniffer.ne")
	//pub := publisher.NewNilPublisher()

	// create the dhcp decoder
	d, err := NewRadiusDecoder(pub)
	if err != nil {
		fmt.Printf("%s\n", err)
		t.Fail()
		return
	}

	var bpf string
	//bpf = d.Bpf()
	//bpf = "( (port 1645 || port 1646)) || mpls"

	// create a packet source
	source, err := packets.MakePacketSourceFromPcap(pcapFile, bpf)
	if err != nil {
		fmt.Printf("%s\n", err)
		t.Fail()
		return
	}

	parser := internal.NewPacketParser()
	for packet := range source.Packets() {

		event, err := parser.Parse(packet.Data())
		if err != nil {
			//fmt.Printf("parse packet error: %s\n", err)
			continue
		}

		if event.IsDHCPv4 == false {
			// decode packet
			done := d.Decode(event)
			_ = done
			//println(done)
		}

	}

	print("Done")

}
