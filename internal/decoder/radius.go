package decoder

import (
	"bitbucket.org/orangeparis/ines/publisher"
	"bitbucket.org/orangeparis/netscan/internal"
	"bitbucket.org/orangeparis/netscan/internal/decoder/radius"
	"fmt"
	"github.com/google/gopacket"
	"log"
)

// // MaxPacketLength is the maximum wire length of a RADIUS packet.
// const MaxPacketLength = 4095

// // Packet is a RADIUS packet.
// type Packet struct {
// 	Code          Code
// 	Identifier    byte
// 	Authenticator [16]byte
// 	Secret        []byte
// 	Attributes
// }

/*
project to handle radius packets

handling radius packets


# protocole :

see https://blog.pingex.net/comprendre-radius-part1-protocole/

UDP / 1813  ( 1812 )
UDP / 1645  ( 1646 )

# verbs:

	ACCESS-REQUEST
	CHALLENGE-REQUEST
	ACCESS-ACCEPT
	ACCESS-REJECT



# attributs:

User-Password: Mot de passe PAP, c’est un attribut assez basique,
Framed-IP-Address: Spécifie une adresse IP à attribuer à l’utilisateur, utilisé dans le cadre de tunnels PPP par exemple,
Tunnel-Type: Donne le type de tunnel utilisé (PPTP, IP-in-IP, GRE, etc.),
Cisco-Call-Type:Un attribut utilisé par le matériel Cisco,
3GPP-IMSI: un numéro unique identifiant un usager sur les réseaux mobiles 2/3/4G (attribut du consortium 3GPP).

*/
//UDP / 1813  ( 1812 )
var radiusName = "radius"
var radiusBpfFilter = "(port 1812||port 1813||port 1815||port 1645||port 1646)||(mpls)"

type RadiusDecoder struct {
	name            string
	ports           []int
	bpfFilter       string
	Publisher       publisher.Publisher
	MessageSequence int
}

func NewRadiusDecoder(pub publisher.Publisher) (decoder *RadiusDecoder, err error) {
	//ports := []int{1645, 1646}
	ports := []int{1812, 1813, 1815, 1645, 1646}
	decoder = &RadiusDecoder{name: radiusName, bpfFilter: radiusBpfFilter, Publisher: pub, ports: ports}

	return
}

// implements decoder interface
func (d *RadiusDecoder) Name() string {
	return d.name
}

func (d *RadiusDecoder) Ports() []int {
	return d.ports
}

func (d *RadiusDecoder) Bpf() string {
	//return ComputeBpf(d.ports)
	return radiusBpfFilter
}

func (d *RadiusDecoder) Decode(event *internal.PacketEvent) bool {

	var radiusClass = ""

	// check we have a radius packet
	if event.SrcPort == 1645 || event.DstPort == 1645 {
		radiusClass = "Authentication"
	} else if event.SrcPort == 1646 || event.DstPort == 1646 {
		radiusClass = "Accounting"
	} else {
		if event.SrcPort == 1812 || event.DstPort == 1812 {
			radiusClass = "Authentication"
		} else if event.SrcPort == 1813 || event.DstPort == 1813 {
			radiusClass = "Accounting"
		} else if event.SrcPort == 1815 || event.DstPort == 1815 {
			radiusClass = "Accounting"
		} else {
			// not a radius packet
			return false
		}
	}

	// this is a radius packet
	event.Kind = "radius"
	event.Event["RadiusClass"] = radiusClass

	payload := event.Layers.Payload
	_ = payload
	parseRadius(event, payload)

	d.PublishEvent(*event)
	return true
}

// private decoder functions
func (d *RadiusDecoder) PublishEvent(event internal.PacketEvent) {

	// compute topic
	var topic string

	// select deviceId  ( srcmac if request  dstmac if reply )
	deviceId := event.SrcMAC
	code := int(event.Layers.Payload[0])
	if code == 2 || code == 3 || code == 5 {
		// replies ( Accept/Reject/Response
		deviceId = event.DstMAC
	}

	// compute topic eg  <device>.radius.AccessRequest
	topic = fmt.Sprintf("%s.radius.%s.%d", deviceId, event.Event["RadiusOp"], d.MessageSequence)
	d.MessageSequence += 1

	msg, err := event.Dumps()
	if err != nil {
		log.Printf("radius failed to marshall event: %s\n", err)
		return
	}
	// publish the dchpv4 event
	err = d.Publisher.Publish(topic, msg)
	if err != nil {
		//
		log.Printf("radius failed to send event: %s\n", err)
	}

}

func (d *RadiusDecoder) Run(source *gopacket.PacketSource) {

	parser := internal.NewPacketParser()
	for packet := range source.Packets() {

		event, err := parser.Parse(packet.Data())
		if err != nil {
			//fmt.Printf("%s\n", err)
			continue
		}

		found := d.Decode(event)
		if found == false {
			//println("not found")
			_ = found
		}
	}
}

// parseRadius: Parse the radius payload
func parseRadius(event *internal.PacketEvent, payload []byte) {

	code := int(payload[0])
	packetId := int(payload[1])

	switch code {
	case 1:
		event.Event["RadiusOp"] = "AccessRequest"
	case 2:
		event.Event["RadiusOp"] = "AccessAccept"
	case 3:
		event.Event["RadiusOp"] = "AccessReject"
	case 4:
		event.Event["RadiusOp"] = "AccountingRequest"
	case 5:
		event.Event["RadiusOp"] = "AccountingResponse"
	default:
		event.Event["RadiusOp"] = "Unknown"
	}

	event.Event["PacketId"] = packetId
	//length := int(payload[2:4])

	dict := radius.Builtin
	secret := "unknown"

	packet, err := radius.Parse(payload, []byte(secret), dict)
	if err != nil {
		log.Printf("Cannot parse radius packet:%s\n", err)
		return
	}

	for _, attr := range packet.Attributes {
		label, ok := packet.Dictionary.Name(attr.Type)
		if ok == true {
			value := attr.Value
			event.Event[label] = value

			switch label {
			case "Vendor-Specific":
				OrangeVendorDecode(attr.Value.([]byte))
				//print(string(value.([]byte)))

			}

		}

	}
	return

}
