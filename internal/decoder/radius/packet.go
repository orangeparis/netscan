package radius

import (
	"encoding/binary"
	"errors"
)

/*
	a simplified version of packet ( only parse )



*/

// maximum RADIUS packet size
const maxPacketSize = 4095

type Referencer interface {
	Codec(byte) AttributeCodec
	Name(t byte) (name string, ok bool)
}

// Code specifies the kind of RADIUS packet
type Code byte

// Codes which are defined in RFC 2865
const (
	CodeAccessRequest Code = 1
	CodeAccessAccept  Code = 2
	CodeAccessReject  Code = 3

	CodeAccountingRequest  Code = 4
	CodeAccountingResponse Code = 5

	CodeAccessChallenge Code = 11

	CodeStatusServer Code = 12
	CodeStatusClient Code = 13

	CodeDisconnectRequest Code = 40
	CodeDisconnectACK     Code = 41
	CodeDisconnectNAK     Code = 42

	CodeCoARequest Code = 43
	CodeCoAACK     Code = 44
	CodeCoANAK     Code = 45

	CodeReserved Code = 255
)

// Packet defines a RADIUS packet.
type Packet struct {
	Code          Code
	Identifier    byte
	Authenticator [16]byte
	Secret        []byte

	Raw        *[]byte
	Dictionary Referencer
	Attributes []*Attribute
}

// Parse parses a RADIUS packet from wire data, using the given shared secret
// and dictionary. nil and an error is returned if there is a problem parsing
// the packet.
//
// Note: this function does not validate the authenticity of a packet.
// Ensuring a packet's authenticity should be done using the IsAuthentic
// method.
func Parse(data, secret []byte, dictionary Referencer) (*Packet, error) {
	if len(data) < 20 {
		return nil, errors.New("radius: packet must be at least 20 bytes long")
	}

	packet := &Packet{
		Code:       Code(data[0]),
		Raw:        &data,
		Identifier: data[1],
		Secret:     secret,
		Dictionary: dictionary,
	}

	length := binary.BigEndian.Uint16(data[2:4])
	if length < 20 || length > maxPacketSize {
		return nil, errors.New("radius: invalid packet length")
	}

	copy(packet.Authenticator[:], data[4:20])

	// Attributes
	attributes := data[20:]
	for len(attributes) > 0 {
		if len(attributes) < 2 {
			return nil, errors.New("radius: attribute must be at least 2 bytes long")
		}

		attrLength := attributes[1]
		if attrLength < 1 || attrLength > 253 || len(attributes) < int(attrLength) {
			return nil, errors.New("radius: invalid attribute length")
		}

		attrType := attributes[0]
		attrValue := attributes[2:attrLength]

		codec := dictionary.Codec(attrType)
		decoded, err := codec.Decode(packet, attrValue)
		if err != nil {
			return nil, err
		}

		attr := &Attribute{
			Type:  attrType,
			Value: decoded,
		}

		packet.Attributes = append(packet.Attributes, attr)
		attributes = attributes[attrLength:]
	}

	// TODO: validate that the given packet (by code) has all the required attributes, etc.
	return packet, nil
}
