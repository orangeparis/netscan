package internal

import (
	"encoding/json"
	"github.com/google/gopacket"
)

// format of the Packet Event
type PacketEvent struct {
	Kind string // kind of event  dhcp/radius

	//case layers.LayerTypeDHCPv4:
	DhcpOp       string `json:"dhcpOp,omitempty"`       //	Dhcp.Operation.String()
	ClientHWAddr string `json:"ClientHWAddr,omitempty"` //  Dhcp.ClientHWAddr.String()

	//case layers.LayerTypeDot1Q:
	VLANIdentifier int    `json:"lanId,omitempty"`    // int (d.Vlan.VLANIdentifier) for sniffer compatibility
	VLANType       string `json:"VLANType,omitempty"` //  type = d.Vlan.Type
	//vlanId = int(vlan.VLANIdentifier)

	//case layers.LayerTypeEthernet:
	SrcMAC string // "] = d.Eth.SrcMAC.String()
	DstMAC string // "] = d.Eth.DstMAC.String()

	//case layers.LayerTypeIPv4 or //case layers.LayerTypeIPv6:

	SrcIP    string // d.Ip4.SrcIP.String() or d.Ip6.SrcIP.String()
	DstIP    string // d.Ip4.DstIP.String() or d.Ip6.DstIP.String()
	IPLength int    // d.Ip4.Length

	//case layers.LayerTypeUDP:
	UdpLength int //  d.Udp.Length
	SrcPort   int //  d.Udp.SrcPort
	DstPort   int //  d.Udp.DstPort

	//case layers.LayerTypeDNS:
	// isDns 	= true

	Event map[string]interface{}

	// added for sniffer v1 compatibility
	Fti string `json:"fti,omitempty"`

	// private properties
	Layers      *Layers               `json:"-"`
	FoundLayers *[]gopacket.LayerType `json:"-"`
	IsDHCPv4    bool                  `json:"-"`
	IsDHCPv6    bool                  `json:"-"`
	IsDNS       bool                  `json:"-"`
}

func (p *PacketEvent) Dumps() ([]byte, error) {
	buffer, err := json.Marshal(p)
	return buffer, err
}

func (p *PacketEvent) Load(data []byte) error {
	err := json.Unmarshal(data, &p)
	return err
}

func NewPacketEvent() *PacketEvent {

	e := &PacketEvent{}
	e.Event = make(map[string]interface{})
	return e

}
