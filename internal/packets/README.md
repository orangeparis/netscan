
# packets 

a factory to build a PacketDataSource from interface or pcap file


to avoid libcap dependency for linux we wanted to use pcapgo

but pcapgo interface is only available  for linux , so we need 2 libraries


a pcapgo library for linux : packets_linux

a gopacket lib for other systems : packets_libcap



# FINALLY

pcapgo DOES NOT IMPLEMENTS BPF filter compilation 

the results will be much too slow:  

so we wont use pcapgo for linux until pcapgo implements ebpf compilation

==> we are stuck to use libcap and wont be able to cross compile it from mac os


Maybe an alternative solution constist of using tcpdump  to compile BPF bytecode

    tcpdump -i en0 -dd "ip and tcp and port 80"


     sniffer_ne master ☢ : sudo tcpdump -i en0 -dd "ip and tcp and port 80"
    { 0x28, 0, 0, 0x0000000c },
    { 0x15, 0, 10, 0x00000800 },
    { 0x30, 0, 0, 0x00000017 },
    { 0x15, 0, 8, 0x00000006 },
    { 0x28, 0, 0, 0x00000014 },
    { 0x45, 6, 0, 0x00001fff },
    { 0xb1, 0, 0, 0x0000000e },
    { 0x48, 0, 0, 0x0000000e },
    { 0x15, 2, 0, 0x00000050 },
    { 0x48, 0, 0, 0x00000010 },
    { 0x15, 0, 1, 0x00000050 },
    { 0x6, 0, 0, 0x00040000 },
    { 0x6, 0, 0, 0x00000000 },

and use the pcapgo instruction

    bpfInstructions := []pcap.BPFInstruction{
    		{0x28, 0, 0, 0x0000000c},
    		{0x15, 0, 10, 0x00000800},
    		{0x30, 0, 0, 0x00000017},
    		{0x15, 0, 8, 0x00000006},
    		{0x28, 0, 0, 0x00000014},
    		{0x45, 6, 0, 0x00001fff},
    		{0xb1, 0, 0, 0x0000000e},
    		{0x48, 0, 0, 0x0000000e},
    		{0x15, 2, 0, 0x00000050},
    		{0x48, 0, 0, 0x00000010},
    		{0x15, 0, 1, 0x00000050},
    		{0x6, 0, 0, 0x00040000},
    		{0x6, 0, 0, 0x00000000},
    	}

    	if err := handle.SetBPFInstructionFilter(bpfInstructions); err != nil {
    		panic(err)
    	}