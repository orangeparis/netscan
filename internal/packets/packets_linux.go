package packets

import (
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcapgo"
	"log"
	"os"
)

/*


MakePacketSourceFromInterface
	get a packetDataSource from an interface via pcapgo ( for linux only )
	so we dont need libcap so we can cross compile it

MakePacketSourceFromPcap

	get a packetDataSource from a pacap file via pcapgo
	so we dont need libcap so we can cross compile it


WARNING : PCAPGO DOES NOT IMPLEMENTS BPF FILTER

	so the solution appears to be MUCH slower than via lib cap

	we can use tcpdump to compile bpf instructions

	tcpdump -i en0 -dd "ip and tcp and port 80"



*/

func MakePacketSourceFromInterface2(ifname string, bpfFilter string) (source *gopacket.PacketSource, err error) {

	handle, err := pcapgo.NewEthernetHandle(ifname)
	if err != nil {
		log.Printf("ifsource_linux.OpenEthernet error: %v", err)
		return
	}
	handle.SetPromiscuous(true)

	//Set BPF filter
	if bpfFilter != "" {

		log.Printf("WARNING: packets_linux does NOT implement bpf filters \n")

		//// try to find a precompiled bpf
		//bpfInstructions, err := GetBpfInstructions(bpfFilter)
		//if len(bpfInstructions) >= 1 {
		//	if err := handle.SetBPFInstructionFilter(bpfInstructions); err != nil {
		//		log.Printf("WARNING: no precompiled bpf found for: %s\n", bpfFilter)
		//	} else {
		//		log.Printf("ifsource set bpf filter to : %s\n", bpfFilter)
		//	}
		//} else {
		//	log.Printf("WARNING: ifsource_linux does NOT implement bpf filters \n")
		//}
		//
		//log.Printf("ifsource set bpf filter to : %s\n", bpfFilter)
	}
	source = gopacket.NewPacketSource(handle, layers.LayerTypeEthernet)
	source.DecodeOptions = gopacket.DecodeOptions{Lazy: true}

	return

}

func MakePacketSourceFromPcap2(pcapfile string, bpfFilter string) (source *gopacket.PacketSource, err error) {

	f, err := os.Open(pcapfile)
	if err != nil {
		log.Printf("sourcelinux: cannot open file %s\n", err)
		return
	}

	options := pcapgo.DefaultNgReaderOptions

	//type NgReaderOptions struct {
	// WantMixedLinkType enables reading a pcapng file containing multiple interfaces with varying link types. If false all link types must match, which is the libpcap behaviour and LinkType returns the link type of the first interface.
	// If true the link type of the packet is also exposed via ci.AncillaryData[0].
	options.WantMixedLinkType = false // bool
	// ErrorOnMismatchingLinkType enables returning an error if a packet with a link type not matching the first interface is encountered and WantMixedLinkType == false.
	// If false packets those packets are just silently ignored, which is the libpcap behaviour.
	options.ErrorOnMismatchingLinkType = false //  bool
	// SkipUnknownVersion enables automatically skipping sections with an unknown version, which is recommended by the pcapng standard. Otherwise ErrVersionMismatch is returned.
	options.SkipUnknownVersion = true // bool
	// SectionEndCallback gets called at the end of a section (execept for the last section, which is ends on EOF). The current list of interfaces and additional section information is provided.
	// This is a good way to read interface statistics.
	//SectionEndCallback func([]NgInterface, NgSectionInfo)
	// StatisticsCallback is called when a interface statistics block is read. The interface id and the read statistics are provided.
	//StatisticsCallback func(int, NgInterfaceStatistics)

	// create reader
	handle, err := pcapgo.NewNgReader(f, options)
	if err != nil {
		log.Printf("cannot create reader:%s\n", err)
		return
	}

	//Set BPF filter
	if bpfFilter != "" {

		log.Printf("WARNING: packets_linux does NOT implements bpf filters\n")

		//// search bpf instructions from precompiled bpf
		//inst, err := GetBpfInstructions(bpfFilter)
		//if err != nil {
		//	log.Printf("WARNING: Cannot find a pre-compiled bpf for %s\n", bpfFilter)
		//} else {
		//	if err := handle.SetBPFInstructionFilter(bpfInstructions); err != nil {
		//		log.Printf("WARNING: failed to apply bpf filter for %s\n", bpfFilter)
		//	} else {
		//		log.Printf("ifsource set bpf (pre-compiled) filter to : %s\n", bpfFilter)
		//	}
		//}
	}
	source = gopacket.NewPacketSource(handle, layers.LayerTypeEthernet)
	source.DecodeOptions = gopacket.DecodeOptions{Lazy: true}
	return
}
