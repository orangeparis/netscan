package packets

import (
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
	"log"
	"time"
)

func MakePacketSourceFromInterface(ifname string, bpf string) (source *gopacket.PacketSource, err error) {

	snapshotLen := int32(1024)
	promiscuous := true
	timeout := time.Duration(30 * time.Second)

	handle, err := pcap.OpenLive(ifname, snapshotLen, promiscuous, timeout)
	if err != nil {
		log.Printf("packets_libcap: cannot open interface %s: %s", ifname, err)
		return
	}

	//Set BPF filter
	if bpf != "" {
		//log.Printf("WARNING: ifsource_linux does NOT implement bpf filters \n")

		err = handle.SetBPFFilter(bpf)
		if err != nil {
			log.Printf(err.Error())
			return
		}
		log.Printf("ifsource set bpf filter to : %s\n", bpf)
	}

	source = gopacket.NewPacketSource(handle, handle.LinkType())
	source.DecodeOptions = gopacket.DecodeOptions{Lazy: true}

	return
}

func MakePacketSourceFromPcap(pcapFile string, bpf string) (source *gopacket.PacketSource, err error) {

	// Open pcapfile
	handle, err := pcap.OpenOffline(pcapFile)
	if err != nil {
		log.Printf("ifsource_libap: cannot open pcap file %s: %s", pcapFile, err)
		return
	}
	//Set BPF filter
	if bpf != "" {

		err = handle.SetBPFFilter(bpf)
		if err != nil {
			log.Printf(err.Error())
			return
		}
		log.Printf("packets: set bpf filter to : %s\n", bpf)
	}

	source = gopacket.NewPacketSource(handle, handle.LinkType())
	source.DecodeOptions = gopacket.DecodeOptions{Lazy: true}
	return
}
