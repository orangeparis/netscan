package packets

import (
	"bitbucket.org/orangeparis/netscan/internal"
	"log"
	"testing"
)

var pcapfile = "../../test/fixtures/ines_dhcp.pcap"

func TestMakePacketSourceFromPcap(t *testing.T) {

	source, err := MakePacketSourceFromPcap(pcapfile, "")
	if err != nil {
		log.Printf("%s\n", err.Error())
		t.Fail()
		return
	}

	p := internal.NewPacketParser()

	for packet := range source.Packets() {
		//ethP := gopacket.NewPacket(data, layers.LayerTypeEthernet, gopacket.Default)

		e, err := p.Parse(packet.Data())
		if err != nil {
			log.Printf("%s\n", err)
			// skip packet
			continue
		}

		payload, err := e.Dumps()
		if err != nil {
			log.Printf("%s\n", err)
			return
		}
		print(string(payload))
		print("\n")

		//// decode a packet

	}

	print("Done.")
}
