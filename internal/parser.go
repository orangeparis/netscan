package internal

import (
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"strings"
)

/*

	a packet parser

	usage :

		parser := NewPacketParser()


		for packet_data := range packets {

			// parse packet to create an event
			event := parser.Parse(packet_data)

			// add custom
			event.add("Property", value)

			json_text,err   := event.Dumps()



*/

// Layers : struct grouping data for parser.DecodeLayers
type Layers struct {
	Ethernet layers.Ethernet
	IPv4     layers.IPv4
	IPv6     layers.IPv6
	UDP      layers.UDP
	TCP      layers.TCP
	Dot1Q    layers.Dot1Q // ( vlan )

	DHCPv4 layers.DHCPv4
	DHCPv6 layers.DHCPv6
	DNS    layers.DNS
	//MPLS   layers.MPLS

	Payload gopacket.Payload
}

type PacketParser struct {
	Parser *gopacket.DecodingLayerParser
	Layers Layers
}

func NewPacketParser() *PacketParser {

	pp := &PacketParser{}

	p := gopacket.NewDecodingLayerParser(
		layers.LayerTypeEthernet,
		&pp.Layers.DHCPv4,
		&pp.Layers.DHCPv6,
		&pp.Layers.Dot1Q,
		&pp.Layers.Ethernet,
		&pp.Layers.IPv4,
		&pp.Layers.IPv6,
		&pp.Layers.TCP,
		&pp.Layers.UDP,
		&pp.Layers.DNS,
		&pp.Layers.Payload,
	)
	pp.Parser = p
	return pp
}

func (p *PacketParser) Parse(data []byte) (event *PacketEvent, err error) {

	var foundLayerTypes []gopacket.LayerType
	err = p.Parser.DecodeLayers(data, &foundLayerTypes)
	if err != nil {
		label := err.Error()
		if strings.Contains(label, "MPLS") != true {
			//log.Printf("[DecodeLayers error   ] %v %v\n", err, foundLayerTypes)
			return
		}
		event, err = p.ParseMplsPacket(data)
		return
	}
	event = NewPacketEvent()
	event.Layers = &p.Layers
	event.FoundLayers = &foundLayerTypes

	for _, layer := range foundLayerTypes {
		switch layer {
		case layers.LayerTypeDHCPv4:
			event.IsDHCPv4 = true
			event.DhcpOp = p.Layers.DHCPv4.Operation.String()
			event.ClientHWAddr = p.Layers.DHCPv4.ClientHWAddr.String()
		case layers.LayerTypeDHCPv6:
			event.IsDHCPv6 = true
			//x := layer.(*layers.DHCPv6)
			//_=x

			// event.DhcpOp = x.Operation.String()
			// event.ClientHWAddr = p.Layers.DHCPv6.ClientHWAddr.String()
		case layers.LayerTypeDot1Q:
			event.VLANIdentifier = int(p.Layers.Dot1Q.VLANIdentifier)
			event.VLANType = p.Layers.Dot1Q.Type.String()
		case layers.LayerTypeEthernet:
			event.SrcMAC = p.Layers.Ethernet.SrcMAC.String()
			event.DstMAC = p.Layers.Ethernet.DstMAC.String()
		case layers.LayerTypeIPv4:
			event.SrcIP = p.Layers.IPv4.SrcIP.String()
			event.DstIP = p.Layers.IPv4.DstIP.String()
			event.IPLength = int(p.Layers.IPv4.Length)
		case layers.LayerTypeIPv6:
			event.SrcIP = p.Layers.IPv6.SrcIP.String()
			event.DstIP = p.Layers.IPv6.DstIP.String()
			event.IPLength = int(p.Layers.IPv6.Length)
		case layers.LayerTypeUDP:
			event.UdpLength = int(p.Layers.UDP.Length)
			event.SrcPort = int(p.Layers.UDP.SrcPort)
			event.DstPort = int(p.Layers.UDP.DstPort)

		case layers.LayerTypeDNS:
			event.IsDNS = true
		}
	}
	return
}

func (p *PacketParser) ParseMplsPacket(data []byte) (event *PacketEvent, err error) {

	x := gopacket.NewPacket(data, layers.LinkTypeEthernet, gopacket.Default)
	if x.ErrorLayer() != nil {
		m := x.ErrorLayer().Error()
		//err := errors.New(m)
		//log.Printf("failed to decode MPLS packet:%s", m)
		return event, m
	}

	if got, ok := x.Layers()[1].(*layers.MPLS); ok {
		_ = got
	}
	if got, ok := x.Layers()[2].(*layers.MPLS); ok {
		_ = got
	}
	event = NewPacketEvent()
	event.Layers = &Layers{}

	for _, layer := range x.Layers() {
		lt := layer.LayerType()
		switch lt {
		case layers.LayerTypeDHCPv4:
			event.IsDHCPv4 = true
			x := layer.(*layers.DHCPv4)
			//event.Layers.DHCPv4 = *x
			event.DhcpOp = x.Operation.String()
			event.ClientHWAddr = p.Layers.DHCPv4.ClientHWAddr.String()
		case layers.LayerTypeDHCPv6:
			event.IsDHCPv6 = true
			x := layer.(*layers.DHCPv6)
			_ = x

			// event.DhcpOp = x.Operation.String()
			// event.ClientHWAddr = p.Layers.DHCPv6.ClientHWAddr.String()
		case layers.LayerTypeDot1Q:
			x := layer.(*layers.Dot1Q)
			//event.Layers.Dot1Q = *x
			event.VLANIdentifier = int(p.Layers.Dot1Q.VLANIdentifier)
			event.VLANType = x.Type.String()
		case layers.LayerTypeEthernet:
			x := layer.(*layers.Ethernet)
			//event.Layers.Ethernet = *x
			event.SrcMAC = x.SrcMAC.String()
			event.DstMAC = p.Layers.Ethernet.DstMAC.String()
		case layers.LayerTypeIPv4:
			x := layer.(*layers.IPv4)
			//event.Layers.IPv4 = *x

			event.SrcIP = x.SrcIP.String()
			event.DstIP = x.DstIP.String()
			event.IPLength = int(x.Length)
		case layers.LayerTypeIPv6:
			x := layer.(*layers.IPv6)
			//event.Layers.IPv6 = *x

			event.SrcIP = x.SrcIP.String()
			event.DstIP = x.DstIP.String()
			event.IPLength = int(x.Length)
		case layers.LayerTypeUDP:
			x := layer.(*layers.UDP)
			//event.Layers.UDP = *x
			event.UdpLength = int(x.Length)
			event.SrcPort = int(x.SrcPort)
			event.DstPort = int(x.DstPort)

		case layers.LayerTypeDNS:
			//x := layer.(*layers.DNS)
			//event.Layers.DNS = *x
			event.IsDNS = true

		case 2:
			// payload
			//c := layer.LayerContents()
			//event.Layers.Payload = make([]byte,len(c))
			//copy(event.Layers.Payload,c)

			event.Layers.Payload = layer.LayerContents()

		}
	}

	return
}
