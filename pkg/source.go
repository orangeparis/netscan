package netscan

import (
	"bitbucket.org/orangeparis/netscan/internal/packets"
	"github.com/google/gopacket"
	"log"
)

// mode == PCAP or INTERFACE
func GetPacketSource(mode string, filename string, bpf string) (source *gopacket.PacketSource, err error) {

	switch mode {
	case "PCAP":
		// read from a pcap file
		log.Printf("sniffer: create a pcap source with %s\n", filename)
		source, err = packets.MakePacketSourceFromPcap(filename, bpf)
		if err != nil {
			log.Printf("sniffer: cannot create a packet source: %s\n", err.Error())
			return
		}

	default:
		// read from network interface (need to be root )
		log.Printf("sniffer : create a device reader with %s\n", filename)
		source, err = packets.MakePacketSourceFromInterface(filename, bpf)
		if err != nil {
			log.Printf("sniffer: cannot create a packet source: %s\n", err.Error())
			return
		}
	}
	return

}
