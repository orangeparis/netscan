package netscan

import (
	"bitbucket.org/orangeparis/ines/publisher"
	"bitbucket.org/orangeparis/netscan/internal/decoder"
	"fmt"
	"log"
)

/*

	factory for pre-builds chain decoder


	usage :

		pub := publisher.NewScreenPublisher("sniffer.0")
		chain,err := netscan.NewDhcpRadiusDecoder(pub)

		bpf := chain.Bpf()
		source, err := netscan.GetPacketSource(mode,name,bpf)

		chain.Run()


*/

func NewDhcpRadiusDecoder(pub publisher.Publisher) (chainDecoder *decoder.Chain, err error) {

	// create the dhcp decoder
	dhcp, err := decoder.NewDhcpV4Decoder(pub)
	if err != nil {
		fmt.Printf("Cannot create Dhcp Decoder: %s\n", err)
		return
	}
	_ = dhcp

	// create a radius decoder
	radius, err := decoder.NewRadiusDecoder(pub)
	if err != nil {
		fmt.Printf("Cannot create Radius Decoder: %s\n", err)
		return
	}
	_ = radius

	// create a decoder chain with dhcp and radius decoder
	chainDecoder = &decoder.Chain{}
	chainDecoder.AddMembers(dhcp, radius)

	return

}

func NewDhcpDecoder(pub publisher.Publisher) (dhcp *decoder.DhcpV4Decoder, err error) {

	// create the dhcp decoder
	dhcp, err = decoder.NewDhcpV4Decoder(pub)
	if err != nil {
		log.Printf("Cannot create Dhcp Decoder: %s\n", err)
		return
	}

	return

}

func NewRadiusDecoder(pub publisher.Publisher) (radius *decoder.RadiusDecoder, err error) {

	// create a radius decoder
	radius, err = decoder.NewRadiusDecoder(pub)
	if err != nil {
		log.Printf("Cannot create Radius Decoder: %s\n", err)
		return
	}

	return

}

func NewDhcpV6Decoder(pub publisher.Publisher) (dhcp *decoder.DhcpV6Decoder, err error) {

	// create the dhcp decoder
	dhcp, err = decoder.NewDhcpV6Decoder(pub)
	if err != nil {
		log.Printf("Cannot create Dhcpv6 Decoder: %s\n", err)
		return
	}

	return

}
