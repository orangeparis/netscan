package _scanners_test

import (
	"bitbucket.org/orangeparis/netscan/scanners"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"log"
	"testing"
)

func TestParser(t *testing.T) {

	myPacketData := []byte{}
	packet := gopacket.NewPacket(myPacketData, layers.LayerTypeEthernet, gopacket.Default)

	dissector := &_scanners.BasicDissector{}
	err := dissector.Parse(packet)
	if err != nil {
		log.Printf("%s\n", err.Error())
	}

}
