package _scanners

import (
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
)

const (
	FOUND    = 0  // the scanner has found a packet => DONT continue with other scanners
	NOTFOUND = 1  // the scanner did found correspong packet => continue with other scanners
	ERROR    = -1 // the scanner has trigger an error => continue with other scanners
)

// Layers : struct grouping data for parser.DecodeLayers
type Layers struct {
	Eth  layers.Ethernet
	Ip4  layers.IPv4
	Ip6  layers.IPv6
	Tcp  layers.TCP
	Udp  layers.UDP
	Dns  layers.DNS
	Body gopacket.Payload
	Vlan layers.Dot1Q
	Dhcp layers.DHCPv4

	VlanId int
}

// the base info we get extract from layers
type LayerData struct {
	/*
	   case layers.LayerTypeDHCPv4:
	   	isDhcp = true
	   case layers.LayerTypeDot1Q:
	   	event["lanId"] = vlan.VLANIdentifier
	   	event["type"] = vlan.Type
	   	vlanId = int(vlan.VLANIdentifier)
	   case layers.LayerTypeEthernet:
	   	event["SrcMAC"] = eth.SrcMAC.String()
	   	event["DstMAC"] = eth.DstMAC.String()
	   case layers.LayerTypeIPv4:
	   	event["SrcIP"] = ip4.SrcIP.String()
	   	event["DstIP"] = ip4.DstIP.String()
	   	event["ip4Len"] = ip4.Length
	   case layers.LayerTypeIPv6:
	   	event["SrcIP"] = ip6.SrcIP.String()
	   	event["DstIP"] = ip6.DstIP.String()
	   case layers.LayerTypeUDP:
	   	event["udpLen"] = udp.Length
	   	event["SrcPort"] = udp.SrcPort
	   	event["DstPort"] = udp.DstPort
	   case layers.LayerTypeDNS:
	   	isDns = true
	*/
}

// packet dissector
// in: packet gopacket.Packet, packetId uint32

type PacketParser interface {
	Parse(packet gopacket.Packet) error
	Layers() *Layers
	Event() *map[string]interface{}
}

type BasicDissector struct {
	Layers
	event map[string]interface{} // store custom event
	//Parser * gopacket.DecodingLayerParser
	//foundLayers []gopacket.LayerType

}

func (d *BasicDissector) Parse(packet gopacket.Packet) (err error) {

	parser := gopacket.NewDecodingLayerParser(
		layers.LayerTypeEthernet,
		&d.Dhcp,
		&d.Vlan,
		&d.Eth,
		&d.Ip4,
		&d.Ip6,
		&d.Tcp,
		&d.Udp,
		&d.Dns,
		&d.Body)

	var foundLayerTypes []gopacket.LayerType
	err = parser.DecodeLayers(packet.Data(), &foundLayerTypes)
	if err != nil {
		//log.Printf("[DecodeLayers error   ] %v %v", err, foundLayerTypes)
		return err
	}
	for _, layer := range foundLayerTypes {
		switch layer {
		case layers.LayerTypeDHCPv4:
			//	isDhcp = true
			d.event["dhcpOp"] = d.Dhcp.Operation.String()
			d.event["ClientHWAddr"] = d.Dhcp.ClientHWAddr.String()
		case layers.LayerTypeDot1Q:
			d.event["lanId"] = d.Vlan.VLANIdentifier
			d.event["type"] = d.Vlan.Type
			//vlanId = int(vlan.VLANIdentifier)
		case layers.LayerTypeEthernet:
			d.event["SrcMAC"] = d.Eth.SrcMAC.String()
			d.event["DstMAC"] = d.Eth.DstMAC.String()
		case layers.LayerTypeIPv4:
			d.event["SrcIP"] = d.Ip4.SrcIP.String()
			d.event["DstIP"] = d.Ip4.DstIP.String()
			d.event["ip4Len"] = d.Ip4.Length
		case layers.LayerTypeIPv6:
			d.event["SrcIP"] = d.Ip6.SrcIP.String()
			d.event["DstIP"] = d.Ip6.DstIP.String()
		case layers.LayerTypeUDP:
			d.event["udpLen"] = d.Udp.Length
			d.event["SrcPort"] = d.Udp.SrcPort
			d.event["DstPort"] = d.Udp.DstPort
			//case layers.LayerTypeDNS:
			//	isDns = true
		}
	}
	return err
}
