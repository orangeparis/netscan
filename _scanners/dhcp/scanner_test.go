package dhcp_test

import (
	"bitbucket.org/orangeparis/ines/publisher"
	filter "bitbucket.org/orangeparis/netscan/scanners/dhcp"
	//"bitbucket.org/orangeparis/sniffer/screenpublisher"
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
	"strings"
	"time"

	"log"
	"testing"
)

/*

	test dhcp scanner with a screenpublisher and the test/fixtures/dhcp.pcap file

*/

var (
	pcapFile string = "../../test/fixtures/dhcp.pcap"

	//handle *pcap.Handle
	//err    error
)

func TestDhcpScanner(t *testing.T) {

	// make a screen publisher
	publisher := publisher.NewScreenPublisher("sniffer.pcap")
	//defer publisher.Close()

	// make packet source
	// Open file instead of device
	handle, err := pcap.OpenOffline(pcapFile)
	if err != nil {
		t.Fail()
		log.Printf("%s\n", err.Error())
		return
	}
	defer handle.Close()

	//// Set BPF filter
	//if bpfFilter != "" {
	//	// eg "portrange 67-68 or  (vlan && portrange 67-68)"
	//	err = handle.SetBPFFilter(bpfFilter)
	//	if err != nil {
	//		log.Printf(err.Error())
	//		return player, err
	//	}
	//	fmt.Printf("bpf filter: %s\n", bpfFilter)
	//}

	// make a Dhcp filter
	// set custom filter
	f := filter.NewFilter(publisher)
	_ = f

	// run it
	log.Printf("entering main HandlePackets loop\n")

	var packetId uint32

	// Loop through packets in file
	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())

	packetId = 0 // (p.id )
	for p := range packetSource.Packets() {

		//spew.Dump(p)

		HandlePacket(p, f, packetId)
		//player.counter++
		packetId++
		// tempo to slow down
		//time.Sleep(1 * time.Millisecond)
	}

	log.Printf("no more packets from source , total: %d\n", packetId)

}

func HandlePacket(packet gopacket.Packet, filter *filter.Filter, id uint32) {

	//log.Printf("==================== PKT [%03d] \n", id)

	event, err := filter.Filter(packet, id)
	_ = event
	if err != nil {
		e := err.Error()
		if strings.HasPrefix(e, "No decoder") {
			// skip error
			//println("skip")
		} else {
			println("filter error result:", err.Error(), "\n")
		}
	} else {
		// success
		//spew.Dump(event)
	}
	// tempo to slow down
	time.Sleep(1 * time.Millisecond)
}
