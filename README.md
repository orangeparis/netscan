# netscan #

a replacement project for sniffer

a nats server to offer scanning network interface services ( dhcp / radius ...)


# what we learn from sniffer

## we need

* a nats service to stat/stop/list several scanner ( dhcp (ex sniffer) , radius )

* a natspublisher with log level option to dump on screen too

* easily add new scanner

* a specific scanner for each type of scan ( dhcp/radius) instead of a global scanner handling multiple protocols

* a config file to start some scanners at service start

# netscan service overview

## resources

    get.netscan.$name
	
a description/status of the netscan service

* general service documentation
* description of available scanners (dhcp/radius ...)
* status of active scanners
* uptime
* stats ?
* localisation (network , site/room/emplacement, vm ...) to be defined
* a link to obtain raml doc
	

	get.netscan.$name.raml
	
a raml doc of the service


	call.netscan.$name.scanner.start
		in: { 
			name: dhcp/radius
			 , }
		
start a scanner

	call.netscan.$name.scanner.clear
	
clear all scanners	
		
	
	call.netscan.$name.$scanner.stop

start a specific scanner

$name : name of the service eg s1 


## events

	event.netscan.$name.uptime
		result: { uptime: int }
	
event emitted periodically by the service itself 

	event.netscan.$name.$device.$scanner.$tag.$sequence
	
events emitted by the scanners while scanning network interface

* name: the name of the service eg s1
* device: an identifier of device implied in the event ( often a mac address )
* scanner: name of the scanner eg dhcp/radius
* tag a tag associated with the scanner eg request/reply for dhp
* a sequence an int incremented with each event based on ($name,$scanner)
	

examples

	event.netscan.s1.84:a1:d1:d3:db:a0.dhcp.request.1
	
	event.netscan.s1.84:a1:d1:d3:db:a0.radius.reply.525


# repository topology

    /cmd
    /internal
        /decoder
        /packets
    /service
   
    
 # sample published messages
 
    screenpublisher: publish to [sniffer.0.a4:7b:2c:e4:d7:77.radius.AccessRequest.50] the message:
    {"Kind":"radius","DhcpOp":"","ClientHWAddr":"","VLANIdentifier":0,"VLANType":"","SrcMAC":"a4:7b:2c:e4:d7:77",
    "DstMAC":"3c:61:04:37:0c:fe","SrcIP":"172.20.23.174","DstIP":"172.20.1.114","IPLength":91,"UdpLength":71,
    "SrcPort":64421,"DstPort":1645,
    "Event":{
        "PacketId":175,
        "RadiusClass":"Authentication",
        "RadiusOp":"AccessRequest"}}
    
    screenpublisher: publish to [sniffer.0.78:94:b4:3d:2a:78.dhcp.Request.91] the message:
    {"Kind":"dhcp","DhcpOp":"Request","ClientHWAddr":"78:94:b4:3d:2a:78","VLANIdentifier":0,"VLANType":"",
    "SrcMAC":"a4:7b:2c:e4:d7:77","DstMAC":"3c:61:04:37:0c:fe","SrcIP":"193.253.21.163","DstIP":"172.20.123.119",
    "IPLength":468,"UdpLength":448,"SrcPort":68,"DstPort":67,
    "Event":{
        "ClientHWAddr":"78:94:b4:3d:2a:78",
        "DHCPAuthentication":"\u0000\u0000\u0000\u001a\t\u0000\u0000\u0005X\u0001\u0003A\u0001\rfti/b2zbpwg\u003c\u0012vv2TgzMx#Xjr!!f@\u0003\u0013*\u001b\ufffd\ufffdQ\u0014D",
        "DHCPCircuitID":"GPON480!0!FTT!1/0/29/0/1:832",
        "DHCPMsgType":"Request",
        "DHCPRemoteID":"00000021349006",
        "VendorClassIdentifier":"sagem",
        "dhcpOp":"Request",
        "fti":"fti/b2zbpwg"}}
    


 
 
    
    
    
    
# open question

as defined we cant open 2 scanners of type dhcp on 2 different interfaces
should we change that ?

should we associate all scanners to same interface defined at the service level ?


should we be able to inject traffic from a pcap file
call.netscan.s1.injector.start
    scanner: dhcp/radius
    pcap file
    