
#  an image to build sniffer

because sniffer use gopacket and go packet needs libcap 
so could not cross compile it from osx




# build image sniffer

    ./build.sh 

# run it interactive

    docker run -it --rm -v ~/app:/app netscan

# build package on local $HOME/app
docker run -it --rm -v ~/app:/app netscan "/go/src/app/mkapp.sh"

## to transfer from container to $HOME/app

    docker run -it --rm -v ~/app:/app netscan
    cp netscan /app/sniffer

then back to the host get

    $HOME/app/sniffer


# build a docker tar archive from a local image

    docker save netscan:latest | gzip > netscan_latest.tgz

# create a local image from a remote tar
    
     docker import http://example.com/netscan_latest.tgz


# build netscan forcing the network mode

    docker build --force-rm --no-cache -t netscan:latest --network=host .
