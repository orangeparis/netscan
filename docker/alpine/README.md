
# an alpine docker netscan

The goal is to provide an alpine docker image for netscan



netscan need gopacket and therefore libcap

we want to make a static executable without the need to have libpcap installed


# maybe a solution at 

    https://github.com/google/gopacket/issues/424
    
    
# build_static.sh

    #!/bin/sh
    set -ex
    apk update
    apk add linux-headers musl-dev gcc go libpcap-dev ca-certificates git
    
    mkdir /go
    export GOPATH=/go
    mkdir -p /go/src/github.com/yourrepo
    mkdir -p /mnt/out
    cp -a /mnt /go/src/github.com/yourrepo/yourtool
    cd /go/src/github.com/yourrepo/yourtool
    rm -f yourtool*
    go get -v ./ ./
    go build --ldflags '-linkmode external -extldflags "-static -s -w"' -v ./
    cp ./yourtool /mnt/out/
    
 Inside the same folder run
 
    docker run --rm=true -itv $PWD:/mnt alpine:3.7 /mnt/build_static.sh
    
 This will give you a static binary inside the out folder
 
 
 # another article : ( the base of the docker file )
 
    see: https://medium.com/@chemidy/create-the-smallest-and-secured-golang-docker-image-based-on-scratch-4752223b7324
    