package service

import (
	"fmt"
	"github.com/jirenius/go-res"
)

//import (
//"errors"
//"fmt"
//"time"
//
//"bitbucket.org/orangeparis/lbservices/config"
//"github.com/jirenius/go-res"
//"github.com/nats-io/go-nats"
//)

/*


	livebox.dora.wait { fti , timeout }  -> { doraEvent }

*/

//
func makeService(name string) (s *res.Service) {

	// create the netscan service  name is like netscan.s1
	s = res.NewService(name)

	// add dora resources
	ScannerResources(s)

	return s

}

func ScannerResources(s *res.Service) {

	// handle netscan.$name.scanner start/clear/stop
	s.Handle("scanner",
		res.Access(res.AccessGranted),

		// set the service  call.netscan.$name.scanner.start
		res.Call("start", func(r res.CallRequest) {
			var p = MsgStart{}
			r.ParseParams(&p)
			scanner := p.Scanner

			if scanner != "dhcp" && scanner != "radius" {
				r.NotFound()
				return
			}
			resp := fmt.Sprintf("netscan: start scanner %s\n", scanner)
			r.OK(resp)
		}),

		// set the service  call.netscan.$name.scanner.stop
		res.Call("stop", func(r res.CallRequest) {
			var p = MsgStart{}
			r.ParseParams(&p)
			scanner := p.Scanner

			if scanner != "dhcp" && scanner != "radius" {
				r.NotFound()
				return
			}
			resp := fmt.Sprintf("netscan: stop scanner %s\n", scanner)
			r.OK(resp)
		}),

		// set the service  call.netscan.$name.scanner.clear
		res.Call("clear", func(r res.CallRequest) {

			resp := fmt.Sprintf("netscan: stop all scanners %s\n")
			r.OK(resp)
		}),
	)

}
