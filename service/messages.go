package service

// MsgStart  message in to call.netscan.$name.start
type MsgStart struct {
	Scanner   string `json:"scanner"`   // scanner name ( dhcp /radius)
	Interface string `json:"interface"` // network interface eg eth1
	Bpf       string `json:"bpf"`       // bpf filter eg ( portrange 67.68 )
}

// ModelHome  the basic status got at  get.netscan.$name
type ModelHome struct {
	Name         string `json:"name"`         //  service name eg s1
	Interface    string `json:"interface"`    // network interface eg eth1
	Localization string `json:"localization"` // vm0234
}
