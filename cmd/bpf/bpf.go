package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

// interface , ( bpf or , ports vlan )

var tcpdump = "/usr/sbin/tcpdump"

var (
	Version = "0.2"

	bpf = flag.String("bpf", "", "bpf expression eg: ip and tcp and port 80")

	networkInterface = flag.String("interface", "en0", "network interface (eg eth1)")
	ports            = flag.String("ports", "", "list of coma separated ports eg 80,8080")

	vlan = flag.Bool("vlan", false, "add vlan to ports")

	helper = `compute the bpf instructions given a bpf expression or list of ports
sample usage : bpf --interface eth1 --bpf "ip and tcp and port 80"
options:
--interface string
    	network interface (default "en0") for the tcpdump command
--bpf string
		bpf expression
--ports string
	comma separated list of ports to create bpf expression
--vlan bool
	add vlan filter for ports

`
)

func main() {

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, fmt.Sprintf("bpf %s\n", Version))
		fmt.Fprintf(os.Stderr, helper)
	}

	flag.Parse()

	var bpf = *bpf
	//var instructions = ""

	if bpf != "" {
		// compute from bpf expression
		instructions, err := bpfInstructions(bpf, *networkInterface)
		if err != nil {
			fmt.Fprintf(os.Stderr, err.Error())
			os.Exit(1)
		}
		fmt.Fprintf(os.Stdout, "%s\n%s", bpf, instructions)
		os.Exit(0)

	} else {
		if *ports == "" {
			fmt.Fprintf(os.Stderr, "specify a bpf or port\n")
			fmt.Fprintf(os.Stderr, helper)
			os.Exit(1)
		}
		// compute from ports
		ports, err := portsFromCommalist(*ports)
		if err != nil {
			fmt.Fprintf(os.Stderr, "bad port format\n")
			os.Exit(1)
		}
		bpf := ComputeBpf(ports)
		instructions, err := bpfInstructions(bpf, *networkInterface)
		if err != nil {
			fmt.Fprintf(os.Stderr, err.Error())
			os.Exit(1)
		}
		fmt.Fprintf(os.Stdout, "%s\n%s", bpf, instructions)
		os.Exit(0)

	}

	println("Done")
}

// use the bpf computing facility of tcpdump
func bpfInstructions(bpf string, iface string) (instructions string, err error) {

	cmd := exec.Command(tcpdump, "-i", iface, "-dd", bpf)
	var stderr bytes.Buffer
	cmd.Stderr = &stderr

	//err := cmd.Run()

	out, err := cmd.Output()
	if err != nil {
		log.Fatal(stderr.String())
		return "", err
	}
	//fmt.Printf("%s\n", out)

	return string(out), nil
}

func ComputeBpf(ports []int) (bpf string) {

	var strPorts []string
	for _, x := range ports {
		strPorts = append(strPorts, "port "+strconv.Itoa(x))
	}
	p := "(" + strings.Join(strPorts, " || ") + ")"
	v := "( vlan && " + p + ")"

	bpf = p + " || " + v
	return bpf
}

func portsFromCommalist(list string) (ports []int, err error) {

	parts := strings.Split(list, ",")
	for _, e := range parts {
		x, err := strconv.Atoi(e)
		if err != nil {
			return ports, err
		}
		ports = append(ports, x)
	}
	return
}
