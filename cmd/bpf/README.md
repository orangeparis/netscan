# bpf helper

a simple tool to compute the pbf instruction


eg 

    bpf --interface en0 --bpf "ip and tcp and port 80"
    
return 
    
    {0x28, 0, 0, 0x0000000c},
    {0x15, 0, 10, 0x00000800},
    {0x30, 0, 0, 0x00000017},
    {0x15, 0, 8, 0x00000006},
    {0x28, 0, 0, 0x00000014},
    {0x45, 6, 0, 0x00001fff},
    {0xb1, 0, 0, 0x0000000e},
    {0x48, 0, 0, 0x0000000e},
    {0x15, 2, 0, 0x00000050},
    {0x48, 0, 0, 0x00000010},
    {0x15, 0, 1, 0x00000050},
    {0x6, 0, 0, 0x00040000},
    {0x6, 0, 0, 0x00000000},
    

or 
    
    bpf --interface --en0 --ports "1635,1636" --vlan
    
    "port 1635 || port 1636 || (vlan && ( port 1635 || port 1636 ))

