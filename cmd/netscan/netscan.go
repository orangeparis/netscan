package main

import (
	netscan "bitbucket.org/orangeparis/netscan/pkg"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"bitbucket.org/orangeparis/ines/heartbeat"
	"bitbucket.org/orangeparis/ines/nping"
	"bitbucket.org/orangeparis/ines/publisher"
)

var (
	Version = "0.2"

	name = flag.String("name", "0", "name of the sniffer (eg 0 , will publish to sniffer.0.>)")

	networkInterface = flag.String("interface", "eth1", "network interface (eg eth1)")
	pcapFile         = flag.String("pcap", "", "pcap file to read (eg: sample.pcap)")

	nats   = flag.String("nats", "nats://127.0.0.1:4222", "nats server (eg nats://demo.nats.io:4222)")
	screen = flag.Bool("screen", false, "publish to screen")

	helper = `Scan a network interface OR a pcap file and send message to nats server OR screen
sample usage : netscan --name s1 --interface eth1 --nats nats://<host>:4222
options:
--name string
	name of the sniffer (default "0") (eg 0 , will publish to sniffer.0.>) 

--interface string
    	network interface (default "eth1")
--pcap string
		pcap file to read (eg: sample.pcap)
		
--nats string
    	nats server (default "nats://127.0.0.1:4222")
--screen
    	publish to screen

`
)

func getPublisher() (pub publisher.Publisher, err error) {

	// return a publisher
	//  if --screen return a screen publisher
	//  else return a nats publisher

	// topic   eg sniffer.0.  sniffer.labo2.
	var topic string

	//if strings.HasSuffix(*name, ".") {
	topic = fmt.Sprintf("sniffer.%s", *name)
	//} else {
	//	topic = fmt.Sprintf("sniffer.%s.", *name)
	//}

	if *screen == true {
		pub = publisher.NewScreenPublisher(topic)
		log.Println("sniffer : set Publisher to screen")

	} else {
		// a nats publisher
		natsURL := *nats
		pub, err = publisher.NewNatsPublisher(topic, natsURL)
		log.Printf("sniffer : set Publisher to nats publisher at %s", natsURL)

		// add a heartbeat emitter Heartbeat.sniffer.1
		full_name := "sniffer." + *name
		e, _ := heartbeat.NewEmitter(full_name, natsURL, 10)
		e.Start()
		log.Printf("sniffer : started a heartbeat emitter [Heartbeat.%s] at  %s", full_name, natsURL)

		// handle call.
		c, err := nping.NewClient(*nats)
		if err != nil {
			log.Printf("Warning: failed to start handler for call.%s.ping\n", topic)
			return pub, err
		}
		defer c.Close()
		if err == nil {
			//pingTopic := "call." + topic + ".ping"
			sub, err := c.HandlePingService(topic)
			if err != nil {
				log.Printf("Warning: failed to subscribe to ping topic: %s\n", topic)
				return pub, err
			} else {
				defer sub.Unsubscribe()
				pingTopic := "call." + topic + ".ping"
				log.Printf("start handler for ping service at: %s\n", pingTopic)

			}

		}

	}
	log.Printf("sniffer : set root topic to: %s\n", topic)

	return pub, err
}

func debugflag() {

}

func run() {

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, fmt.Sprintf("sniffer %s\n", Version))
		fmt.Fprintf(os.Stderr, helper)
	}

	flag.Parse()

	// get a publisher corresponding to the flags ( screen or nats )
	pub, err := getPublisher()
	if err != nil {
		log.Fatalf("sniffer: cannot get a publisher: %s\n", err.Error())
		os.Exit(1)
	}

	// build decoder chain
	chain, err := netscan.NewDhcpRadiusDecoder(pub)
	if err != nil {
		log.Fatalf("Cannot create chain decoder :%s\n", err)
	}
	bpf := chain.Bpf()

	// find  mode ( interface or pcap )
	mode := "INTERFACE"
	name := *networkInterface
	if *pcapFile != "" {
		mode = "PCAP"
		name = *pcapFile
	}

	src, err := netscan.GetPacketSource(mode, name, bpf)
	if err != nil {
		log.Fatalf("sniffer: cannot create a packet source: %s\n", err.Error())
	}
	chain.Run(src)
	time.Sleep(5 * time.Second)

	log.Printf("sniffer : exiting")

}

func main() {
	run()
}
