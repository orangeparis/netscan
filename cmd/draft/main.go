package main

import (
	"encoding/json"
	"log"
)

type Core struct {
	P1 string

	Event interface{}
}

func (c *Core) Unmarshall(data []byte) (err error) {
	return json.Unmarshal(data, &c)
}

func (c *Core) marshall() (data []byte, err error) {
	j, err := json.Marshal(&c)
	return j, err
}

type Message struct {
	Properties
	Event interface{}
}

type Properties struct {
	P1 string
}

type Event1 struct {
	E1 string
}
type Event2 struct {
	E2 string
}

func NewEvent1() *Core {
	return &Core{Event: &Event1{}}
}

func NewEvent2() *Core {
	return &Core{Event: Event2{}}
}

func simple() {

	e1 := &Event1{E1: "event one"}
	e2 := &Event2{E2: "event two"}

	o1 := &Core{P1: "objet 1", Event: e1}
	o2 := &Core{P1: "objet 2", Event: e2}

	// marshall o1
	j1, err := o1.marshall()
	if err != nil {
		log.Printf("%s\n", err)
		return
	}

	// marshall o2
	j2, err := o2.marshall()
	if err != nil {
		log.Printf("%s\n", err)
		return
	}

	// extract o1
	x1 := &Core{}
	err = x1.Unmarshall(j1)
	if err != nil {
		log.Printf("%s\n", err)
		return
	}
	// marshall and compare to origin
	xj1, err := x1.marshall()
	if err != nil {
		log.Printf("%s\n", err)
		return
	}
	if string(j1) != string(xj1) {
		log.Printf("difference : %s\n%s\n", j1, xj1)
	}

	// extract o2
	x2 := &Core{}
	err = x2.Unmarshall(j2)
	if err != nil {
		log.Printf("%s\n", err)
		return
	}
	// marshall and compare to origin
	xj2, err := x2.marshall()
	if err != nil {
		log.Printf("%s\n", err)
		return
	}
	if string(j2) != string(xj2) {
		log.Printf("difference : %s\n%s\n", j1, xj1)
	}

}

func enveloppe() {

	m1 := &Message{}
	m1.P1 = "p1"

	event := &Event1{}
	event.E1 = "event one"

	m1.Event = event

}

func main() {

	simple()
	enveloppe()

	println("done")

}
