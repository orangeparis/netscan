# netscan 2


a sniffer reading from an interface and publishing nats events

handle dhcp and radius with 2 different goroutines because dhcp needs bpf vlan filter
and radius needs mpls bpf filter and it seems to incompatible in the same bpf filter



# dhcp


## source
read interface or pcap file

bpf: ( port 67  || port 68)  || (vlan && ( port 67 || port 68 )


## messages

    sniffer.<interface>.<mac>.dhcp.<tag>.<sequence>

    eg sniffer.ne.78:94:b4:3d:2a:78.dhcp.Request.91

# radius

## source

read interface or pcap file

bpf:  ()

messages

    sniffer.<interface>.<mac>.radius.<tag>.<sequence>

    eg sniffer.ne.a4:7b:2c:e4:d7:77.radius.AccessRequest.1




